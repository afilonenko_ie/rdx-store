// TODO build and iterator
// TODO intersect relations by variable
// TODO generic join algorithm
// TODO fail-first single-set join
// TODO deal with a fixed order stuff
import _ from 'lodash';
import pull from 'pull-stream';
import {
  makeState,
  encodeTriple,
  decodeTriple,
  encodeTerm,
  decodeTerm,
  search,
  add
} from './core';

import { getNextValueOrNull, makeIterator } from './lazyUtil';

global.decodeTriple = decodeTriple;
global.search = search;

function identity(x) {
  return x;
}

function triple(s, p, o) {
  return { s, p, o };
}

function pairsToObject(pairs) {
  return pairs.reduce(([k, v], res) => Object.assign(res, { [k]: v }), {});
}

const data = [
  // x R y
  triple(0, 'R', 1),
  triple(2, 'R', 0),
  triple(2, 'R', 3),
  triple(2, 'R', 5),
  triple(3, 'R', 4),
  triple(5, 'R', 6),
  triple(4, 'R', 2),

  // y R z
  triple(0, 'S', 1),
  triple(2, 'S', 0),
  triple(2, 'S', 3),
  triple(2, 'S', 5),
  triple(3, 'S', 4),
  triple(4, 'S', 2),
  triple(5, 'S', 6),

  // x R z
  triple(0, 'T', 2),
  triple(1, 'T', 0),
  triple(2, 'T', 4),
  triple(3, 'T', 2),
  triple(4, 'T', 3),
  triple(5, 'T', 2),
  triple(6, 'T', 5)
];

function arrayIter(array) {

  let position = -Infinity;
  const iterator = array[Symbol.iterator]();

  function next() {
    const { done, value } = iterator.next();
    position = done ? Infinity : value;
    return { done, value };
  }

  return {
    get position() { return position; },
    [Symbol.iterator]: () => ({ next }),
    next,
    jump(border) {
      let _done;
      if (border <= position) return arrayIter(array).jump(border);
      while (!_done) {
        const { done, value } = this.next();
        _done = done;
        position = value;
        if (value >= border) return value;
      }
      return Infinity;
    }
  };
}

const intersectTestData = [
  [0, 1, 3, 4, 5, 6, 7, 8, 9, 11],
  [0, 2, 6, 7, 8, 9, 12],
  [0, 2, 4, 5, 8, 9]
]
  .map(arrayIter);


global.intersectTestData = intersectTestData;
global.arrayIter = arrayIter;

const empty = makeState();

const database = data.reduce(add, empty);

global.database = database;


class Subgoal {
  constructor(constants, variables) {
    this.constants = constants;
    this.variables = variables;
    this.arity = Object.keys(variables).length;
  }

  get clause() {
    if (this._clause) return this._clause;
    const s = this.constants.s
      ? this.constants.s
      : `?${this.variables.s}`;
    const p = this.constants.p
      ? this.constants.p
      : `?${this.variables.p}`;
    const o = this.constants.o
      ? this.constants.o
      : `?${this.variables.o}`;
    this._clause = { s, p, o };
    return this._clause;
  }

  getVariableAttr(variable) {
    return Object
      .entries(this.variables)
      .find(([k, v]) => v === variable)[0]; // eslint-disable-line no-unused-vars
  }

  supports(variable) {
    return Object.values(this.variables).includes(variable);
  }

  tighten(tuple) {
    const consts = { ...this.constants, ...tuple };
    const vars = {};
    Object.entries(this.variables).forEach(([k, v]) => {
      if (!Object.keys(tuple).includes(k)) {
        vars[k] = v;
      }
    });
    return new Subgoal(consts, vars);
  }

  toString() {
    const { s, p, o } = this.clause;
    return `${s} ${p} ${o}`;
  }
}

function accessRawIndex(source, indexName) {
  return source.get('permutations').get(indexName);
}

class Index {
  constructor(source, name) {
    this.asArray = [...name];
    this.source = source;
    this.name = name;
  }

  encodeTerm(term) {
    return encodeTerm(this.source, term);
  }

  getLeastUpperBound(subgoal, bound) {
    const key = Array.isArray(bound) ? bound : [bound];
    const prefix = this.asArray.slice(0, 3 - subgoal.arity).map(attribute =>
      this.encodeTerm(subgoal.constants[attribute])
    );
    const infinity = Array.from({ length: 3 - subgoal.arity }).map(() => Infinity);
    const from = [...prefix, ...key];
    const to = [...prefix, ...infinity];
    const iter = accessRawIndex(this.source, this.indexName).entries({
      from,
      to
    }).next();
    return getNextValueOrNull(iter);
  }

  toString() {
    return this.name;
  }
}

class Relation {
  constructor(source, subgoal, index) {
    this.subgoal = subgoal;
    this.source = source;
    this.index = index;
  }

  supports(variable) {
    return this.subgoal.supports(variable);
  }

  tighten(tuple) {
    return new Relation(this.source, this.subgoal.tighten(tuple), this.index);
  }

  toString() {
    return `<${this.subgoal}> <- ${this.index.name}(${this.source.name || '$'})`;
  }
}

function selectIteratorIndex(constants, variableAttr) {
  const { s, p, o } = constants;
  let prefix;
  // constant attribute should come first
  if (s && !p && !o) prefix = ['s'];
  else if (!s && p && !o) prefix = ['p'];
  else if (!s && !p && o) prefix = ['o'];
  else if (s && p && !o) prefix = ['s', 'p'];
  else if (s && !p && o) prefix = ['o', 's'];
  else if (!s && p && o) prefix = ['p', 'o'];
  else if (!s && !p && !o) prefix = [];
  else return 'spo';
  const indexArr = prefix.concat(variableAttr);
  if (indexArr.length === 3) return indexArr.join('');
  const lastAttrs = ['s', 'p', 'o'].filter(a => !indexArr.includes(a));
  return {
    searchIndexName: indexArr.concat(lastAttrs).join(''),
    prefix
  };
}


function makeRelationIterator(relation, variable, position = -Infinity) {

  let _position = position;
  const db = relation.source;
  const constants = relation.subgoal.constants;
  const variableAttr = relation.subgoal.getVariableAttr(variable);
  const { searchIndexName, prefix }
    = selectIteratorIndex(relation.subgoal.constants, variableAttr);
  const iterator = search(db, searchIndexName, constants)[Symbol.iterator]();

  /**
   * Find a next element in an index
   *
   * @return value of relation index at next point
   */
  function next() {
    _position = iterator.next();
    return _position;
  }

  /**
   * Jumps to a position near the search key
   */
  function jump(border) {
    const decodedBorder = encodeTerm(db, border);
    // create a search key
    const searchKeyPrefix = prefix
      .map(prefixAttr => encodeTerm(db, constants[prefixAttr]));
    const from = searchKeyPrefix
      .concat(decodedBorder)
      .concat(Array.from({ length: 2 - prefix.length }).map(() => -Infinity));
    const to = searchKeyPrefix
      .concat(Array.from({ length: 3 - prefix.length }).map(() => Infinity));
    // perform search
    const encodedRes = db
      .getIn(['permutations', searchIndexName])
      .values({ from, to })
      .next().value;
    // decode result
    const res = decodeTriple(db, encodedRes);
    // store it as position
    _position = res;
    return res;
  }

  return {
    get position() { return position; },
    [Symbol.iterator]: () => ({ next }),
    next,
    jump
  };
}
global.makeRelationIterator = makeRelationIterator;

export function _selectIndexName(subgoal, variables) {
  return ['p', 's', 'o']
    .map(attr => {
      if (subgoal.constants[attr]) return { attr, order: -1 };
      const subgoalVariable = subgoal.variables[attr];
      const order = variables.indexOf(subgoalVariable);
      return { attr, order };
    })
    .sort((a, b) => a.order - b.order)
    .map(x => x.attr)
    .join('');
}

class Query {

  static fromTriql(q, source) {
    const headVariables = q.select;
    const variables = [];
    const subgoals = q.where.map(clause => {
      const { consts, vars } = [
        ['s', clause[0]],
        ['p', clause[1]],
        ['o', clause[2]]
      ].reduce((res, [k, v]) => {
        if (v.startsWith('?')) {
          const _v = v.substring(1);
          res.vars[k] = _v;
          variables.push(_v);
          return res;
        }
        res.consts[k] = v;
        return res;
      }, { consts: {}, vars: {} });
      return new Subgoal(consts, vars);
    });
    const relations = subgoals.map(subgoal => {
      const index = new Index(source, _selectIndexName(subgoal, variables));
      return new Relation(source, subgoal, index);
    });
    return new Query(variables, relations, headVariables);
  }

  constructor(variables, relations, headVariables = []) {
    this.variables = variables; this.relations = relations; this.headVariables = headVariables;
  }

  tighten(variable, value) {
    // remove variable from a variable list
    const variables = this.variables.filter(v => v !== variable);
    const relations = this.relations
      .filter(relation => relation.supports(variable))
      .map(relation => relation.tighten(value));
    return new Query(variables, relations);
  }
}


function* intersectBy(iterators, accessor = identity) {
  let p = 0; // position pointer
  let prev = 0;
  const k = iterators.length;
  const matching = [];

  for (;;) {
    const iter = iterators[p % k];
    const value = iter.jump(prev);
    if (value === Infinity) break;
    const pos = accessor(value);
    if (prev === pos) {
      matching[p % k] = value;
    } else {
      matching.length = 0;
    }
    p++;
    const matchSuccess = Object.keys(matching).length === k;
    if (matchSuccess) {
      yield Array.from(matching);
      matching.length = 0;
      const prevVal = iterators[(p + 1) % k].next();
      if (prevVal.done) break;
      prev = prevVal.value;
    } else {
      prev = pos;
    }
  }
}

global.intersectBy = intersectBy;


function* multijoin(vars, rels, tuple) {
  if (vars.length === 1) {
    yield intersectBy(rels.map(r => r.tighten(tuple)));
  } else {
    const v = vars[0];
    const branch = [v];
    const rest = vars.filter(_v => _v !== vars[0]);
    const projections = rels
      .filter(r => r.supports(v))
      .map(r => r.tighten(tuple).project(branch));
    for (const extension of intersectBy(projections)) {
      const partialQuery = multijoin(rest, rels, { tuple, extension });
      yield partialQuery.map(pq => ({ ...pq, extension }));
    }
  }
}

global.multijoin = multijoin;

const q = {
  select: ['x', 'y', 'z'],
  where: [
    ['?x', 'R', '?y'],
    ['?y', 'S', '?z'],
    ['?x', 'T', '?z']
  ]
};

const triangularQuery = Query.fromTriql(q, database);
global.r0 = triangularQuery.relations[0];
global.triangularQuery = triangularQuery;

