// TODO: write tests
// TODO: implement immutable.js interfaces
// TODO: batching support
// TODO: move dictionary to separate class

import { BTMap } from 'ibtree';
import { Map } from 'immutable';
import { map } from './lazyUtil';

const PERMUTATIONS = ['spo', 'sop', 'pso', 'pos', 'ops', 'osp'];
const AGGREGATE_INDICES = ['s', 'o', 'p', 'sp', 'so', 'ps', 'po', 'op', 'os'];

function tripleIsEmpty({ s, p, o }) {
  return !s && !p && !o;
}

function tripleIsComplete({ s, p, o }) {
  return s != null && p != null && o != null;
}
global.tripleIsComplete = tripleIsComplete;

function indexToJs(index) {
  return [...index.entries()].reduce((memo, [k, v]) => Object.assign(memo, { [k]: v }), {});
}

export function stateToJs(state) {
  const permutations = PERMUTATIONS.reduce((memo, indexName) => {
    const index = indexToJs(state.getIn(['permutations', indexName]));
    return Object.assign(memo, { [indexName]: index });
  }, {});
  const jsState = state.toJS();
  jsState.permutations = permutations;
  return jsState;
}
global.stateToJs = stateToJs;

function compareAlphaNum(a, b) {
  if (typeof a !== 'number' && typeof b !== 'number') return a > b ? 1 : -1;
  if (typeof a !== 'number' && typeof b === 'number') return -1;
  if (typeof a === 'number' && typeof b !== 'number') return 1;
  return a - b;
}

function comparatorDecode(code2term) {
  return x => {
    if (x === Infinity) return x;
    if (x === -Infinity) return x;
    return code2term.get(x);
  };
}

const comparator = code2term => (a, b) => {
  if (a === undefined && b === undefined) return 0;
  if (a === undefined && b !== undefined) return -1;
  if (a !== undefined && b === undefined) return 1;
  if (a === -Infinity) return -1;
  if (b === Infinity) return 1;
  const decode = comparatorDecode(code2term);
  const _a = a.map(decode);
  const _b = b.map(decode);
  if (_a[0] !== _b[0]) return compareAlphaNum(_a[0], _b[0]);
  if (_a[1] !== _b[1]) return compareAlphaNum(_a[1], _b[1]);
  if (_a[2] - _b[2]) return compareAlphaNum(_a[2], _b[2]);
  return 0;
};

function makeIndex(code2term) {
  return new BTMap({ comparator: comparator(code2term) });
}

export function makeState() {
  const aggregates = Map(AGGREGATE_INDICES.map(indexName => [indexName, Map()]));
  const code2term = Map();
  const term2code = Map();
  const permutations = Map(PERMUTATIONS.map(indexName => [indexName, makeIndex(code2term)]));
  return Map({
    clock: 0,
    code2term,
    term2code,
    permutations,
    aggregates
  });
}

function addTermToState(state, part) {
  const oldCode = state.get('term2code').get(part);
  if (oldCode) return { state, code: oldCode };
  const clock = state.get('clock');
  const code = clock + 1;
  return {
    state: state
      .setIn(['code2term', code], part)
      .setIn(['term2code', part], code)
      .set('clock', code),
    code
  };
}


function getPermutation(state) {
  return permutation => state.getIn(['permutations', permutation]);
}

export function decodeTriple(state, encodedTriple) {
  const codes = state.get('code2term');
  const { s, p, o } = encodedTriple;
  return {
    s: codes.get(s),
    p: codes.get(p),
    o: codes.get(o)
  };
}

export function encodeTriple(state, triple) {
  const codes = state.get('term2code');
  const { s, p, o } = triple;
  return {
    s: codes.get(s),
    p: codes.get(p),
    o: codes.get(o)
  };
}

export function encodeTerm(state, term) {
  return state.getIn(['term2code', term]);
}

export function decodeTerm(state, code) {
  return state.getIn(['code2term', code]);
}

export function selectSearchIndex({ s, p, o }) {
  if (s && !p && !o) return 'spo';
  if (!s && p && !o) return 'pos';
  if (!s && !p && o) return 'osp';

  if (s && p && !o) return 'spo';
  if (s && !p && o) return 'osp';
  if (!s && p && o) return 'pos';

  return 'spo';
}

function getRangeAndLengthKey(pattern, triple) {
  const from = [];
  const to = [];
  let aggregateIndexName = '';
  const aggregateIndexValueKey = [];
  for (const v of [...pattern]) {
    if (triple[v] != null) {
      const val = triple[v];
      from.push(triple[v]);
      aggregateIndexName += v;
      aggregateIndexValueKey.push(val);
      to.push(val);
    } else {
      from.push(-1);
      to.push(Infinity);
    }
  }
  return {
    range: { from, to },
    aggregateKey: [
      'aggregates',
      aggregateIndexName,
      aggregateIndexValueKey.length === 1
        ? aggregateIndexValueKey[0]
        : aggregateIndexValueKey.join(',')
    ]
  };
}

function searchEncoded(state, indexName, triple) {
  if (tripleIsEmpty(triple)) return [];
  const { range, aggregateKey } = getRangeAndLengthKey(indexName, triple);
  const res = getPermutation(state)(indexName).values(range);
  res.length = state.getIn(aggregateKey) | 0;
  return res;
}

export function search(state, indexName, triple) {
  let _indexName = indexName;
  let _triple = triple;
  if (!triple) {
    _triple = indexName;
    _indexName = selectSearchIndex(_triple);
  }
  const encoded = encodeTriple(state, _triple);
  const isTripleComplete =
    Object.values(encoded).filter(x => x).length !== Object.keys(_triple).length;
  if (isTripleComplete) return [];
  const encodedSearchResult = searchEncoded(state, _indexName, encoded);
  const res = map(_encoded => decodeTriple(state, _encoded), encodedSearchResult);
  res.length = encodedSearchResult.length;
  return res;
}

function increment(x) { return (x | 0) + 1; }
function decrement(x) { return (x | 0) - 1; }

function updateAggregationCounters(updateFn, state, triple) {
  return AGGREGATE_INDICES.reduce((newState, indexName) => {
    const aggregateKey = indexName.length === 1
      ? triple[indexName]
      : [...indexName].map(value => triple[value]).join(',');
    const key = ['aggregates', indexName, aggregateKey];
    const newValue = updateFn(state.getIn(key));
    return newValue < 1 ? state.deleteIn(key) : state.setIn(key, newValue);
  }, state);
}

export function has(state, triple) {
  const encoded = encodeTriple(state, triple);
  const { s, p, o } = encoded;
  return tripleIsComplete(encoded) && state.getIn(['permutations', 'spo']).has([s, p, o]);
}

export function add(state, triple) {
  const { s, p, o } = triple;
  if (has(state, triple)) return state;
  return state.withMutations(mutableState => {
    const encoded = {
      s: addTermToState(mutableState, s).code,
      p: addTermToState(mutableState, p).code,
      o: addTermToState(mutableState, o).code
    };
    updateAggregationCounters(increment, mutableState, encoded);
    PERMUTATIONS.forEach(indexName => {
      const key = [];
      for (const element of indexName) {
        key.push(encoded[element]);
      }
      mutableState.updateIn(['permutations', indexName], permutationIndex => {
        permutationIndex.comparator = comparator(mutableState.get('code2term'));
        return permutationIndex.set(key, encoded)
      });
    });
    return mutableState;
  });
}

function getDeleteClockIncrement(state, encodedTriple) {
  return [...'spo'].filter(termName => {
    const key = ['aggregates', termName, encodedTriple[termName]];
    return state.getIn(key) < 2;
  }).length;
}

function removeTripleFromValueMap(state, triple, encoded) {
  return ['s', 'p', 'o'].reduce((memo, componentName) => {
    const encodedTerm = encoded[componentName];
    const refCount = state.getIn(['aggregates', componentName, encodedTerm]);
    if (refCount <= 1) return state;
    const t2cKey = ['term2code', triple[componentName]];
    const c2tKey = ['code2term', encodedTerm];
    return state.deleteIn(t2cKey).deleteIn(c2tKey);
  }, state);
}

export function del(state, triple) {
  const encoded = encodeTriple(state, triple);
  const incrementValue = getDeleteClockIncrement(state, encoded);
  return state.withMutations(mutableState => {
    updateAggregationCounters(decrement, mutableState, encoded);
    mutableState.update('clock', c => c + incrementValue);
    for (const permutation of PERMUTATIONS) {
      const key = [...permutation].map(attr => encoded[attr]);
      mutableState.updateIn(['permutations', permutation], permutationIndex => (
        permutationIndex.delete(key)
      ));
    }
    removeTripleFromValueMap(mutableState, triple, encoded);
    return mutableState;
  });
}

export default {
  makeState,
  has,
  add,
  del,
  search
};
