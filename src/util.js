export function pipe(fn, ...fns) {
  return (...args) => fns.reduce((acc, _fn) => _fn(acc), fn(...args));
}

export function isPrimitive(value) {
  return value == null
   || value instanceof Date
   || (typeof value !== 'function' && typeof value !== 'object');
}
