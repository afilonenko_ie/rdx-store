export class RDFTerm {

  constructor(value) { this.value = value; }

  toString() { return this.value.toString(); }

}

export class Literal extends RDFTerm {

  termType = 'literal'
  static from(value) { return Object.freeze(new Literal(value)); }

  toString() {
    return `"${this.value}"`;
  }
}

export class IRI extends RDFTerm {

  termType = 'IRI'
  static from(value) { return Object.freeze(new IRI(value)); }

  toString() {
    return `<${this.value}>`;
  }
}

export class BlankNode extends RDFTerm {
  termType = 'blankNode';

  constructor(value, count) {
    super(value);
    this.count = count;
  }

  static from(value, id) { return Object.freeze(new BlankNode(value, id)); }
}

