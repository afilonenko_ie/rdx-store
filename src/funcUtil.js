export function pipe(fn, ...fns) {
  return (...args) => fns.reduce((acc, _fn) => _fn(acc), fn(...args));
}

