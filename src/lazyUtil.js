export function map(f, iter) {

  const iterable = (typeof iter.next === 'function')
    ? iter
    : iter[Symbol.iterator]();

  function iterator() {
    return {
      next() {
        const { done, value } = iterable.next();
        return { done, value: done ? undefined : f(value) };
      }
    };
  }

  return {
    [Symbol.iterator]: iterator
  };

}

export function makeItererator(next) {
  return {
    next,
    [Symbol.iterator]: () => ({ next })
  };
}

export function getIterator(iterable) {
  return iterable[Symbol.iterator]();
}

export function take(iterable, n) {
  let _n = n;
  const iterator = getIterator(iterable);
  return makeItererator(() => {
    const { done, value } = iterator.next();
    if (done || _n-- === 0) return { done: true };
    return { value };
  });
}

export function getNextValueOrNull(iter) {
  const { done, value } = iter.next();
  return done ? null : value;
}
