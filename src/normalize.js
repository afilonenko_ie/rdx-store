import pull from 'pull-stream';
import uuid from 'uuid';

import { Literal, IRI, BlankNode } from './term';
import Triple from './triple';
import { isPrimitive } from './util';

global.pull = pull;
global.uuid = uuid;

export function entries(obj) {
  return Object.keys(obj).map(key => [key, obj[key]]);
}

export const _obj = {
  "name": "Mojito",
  "ingredient": [
    "12 fresh mint leaves",
    "1/2 lime, juiced with pulp",
    "1 tablespoons white sugar",
    "1 cup ice cubes",
    "2 fluid ounces white rum",
    "1/2 cup club soda"
  ],
  "yield": "1 cocktail",
  "instructions": [
    {
      "step": 1,
      "description": "Crush lime juice, mint and sugar together in glass."
    },
    {
      "step": 2,
      "description": "Fill glass to top with ice cubes."
    },
    {
      "step": 3,
      "description": "Pour white rum over ice."
    },
    {
      "step": 4,
      "description": "Fill the rest of glass with club soda, stir."
    },
    {
      "step": 5,
      "description": "Garnish with a lime wedge."
    }
  ]
};


// Depth First Walk
function _normalize(obj) {
  const contextProfix = '_:b';
  let contextCounter = -1;

  function nextNodeId() {
    return `${contextProfix}${++contextCounter}`;
  }

  const stack = [{
    value: obj,
    name: BlankNode.from(nextNodeId(), contextCounter)
  }];

  function next(end, cb) {
    const results = [];

    if (end) return cb(end);
    if (!stack.length) return cb(true);
    const v = stack.pop();
    v.wasVisited = true;
    if (isPrimitive(v.value)) return cb(null);
    entries(v.value).forEach(([key, value]) => {
      if (isPrimitive(value)) {
        return results.push(Triple.fromSPO(
          v.name, IRI.from(key), Literal.from(value)
        ));
      }
      if (Array.isArray(value)) {
        return value.forEach(item => {
          if (isPrimitive(item)) {
            return results.push(Triple.fromSPO(
              v.name, IRI.from(key), Literal.from(item)
            ));
          }
          const nodeName = BlankNode.from(nextNodeId(), contextCounter);
          stack.push({ name: nodeName, value: item });
          return results.push(Triple.fromSPO(
            v.name,
            IRI.from(key),
            nodeName
          ));
        });
      }
      const nodeName = BlankNode.from(nextNodeId(), contextCounter);
      const triple = Triple.fromSPO(v.name, IRI.from(key), nodeName);
      stack.push({ name: nodeName, value });
      return results.push(triple);
    });

    return cb(null, results);
  }

  return next;
}

export function normalize(obj) {
  return pull(_normalize(obj), pull.filter(x => x.length), pull.flatten());
}

pull(normalize(_obj), pull.collect((err, res) => {
  if (err) throw err;
  global.triples = res;
}));

export function toRDF(triples) {
  return triples.map(triple => triple.toString()).join(' .::').replace(/::/g, '\n');
}

console.log(toRDF(triples));

Object.keys(module.exports).forEach(exp => global[exp] = module.exports[exp]);
