export default class Triple {

  static fromSPO(s, p, o) {
    return Object.freeze(new Triple({ s, p, o }));
  }

  static from({ s, p, o }) {
    return Object.freeze(new Triple({ s, p, o }));
  }

  constructor({ s, p, o }) { this.s = s; this.p = p; this.o = o; }

  toObject() {
    return {
      s: this.s.toString(),
      p: this.p.toString(),
      o: this.o.toString()
    };
  }

  toString() {
    const s = this.s.toString();
    const p = this.p.toString();
    const o = this.o.toString();
    return `${s} ${p} ${o}`;
  }
}

