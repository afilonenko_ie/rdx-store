import { test } from 'tape';
import { isImmutable } from 'immutable';
import _, { difference } from 'lodash';
import { BTMap } from 'ibtree';
import { makeState, add, search, has, del, stateToJs } from '../src/core';

global._ = _;

global.entries = Object.entries;

const triples = [
  ['_:b31', '<unconnected>', '<node>'],
  ['_:b0', '<name>', '"Mojito"'],
  ['_:b0', '<description>', '"Mojito coctail"'],
  ['_:b0', '<ingredient>', '"12 fresh mint leaves"'],
  ['_:b0', '<ingredient>', '"1/2 lime, juiced with pulp"'],
  ['_:b0', '<ingredient>', '"1 tablespoons white sugar"'],
  ['_:b0', '<ingredient>', '"1 cup ice cubes"'],
  ['_:b0', '<ingredient>', '"2 fluid ounces white rum"'],
  ['_:b0', '<ingredient>', '"1/2 cup club soda"'],
  ['_:b0', '<yield>', '"1 cocktail"'],
  ['_:b0', '<instructions>', '_:b1'],
  ['_:b0', '<instructions>', '_:b2'],
  ['_:b0', '<instructions>', '_:b3'],
  ['_:b0', '<instructions>', '_:b4'],
  ['_:b0', '<instructions>', '_:b5'],
  ['_:b5', '<step>', '"5"'],
  ['_:b5', '<description>', '"Garnish with a lime wedge."'],
  ['_:b4', '<step>', '"4"'],
  ['_:b4', '<description>', '"Fill the rest of glass with club soda, stir."'],
  ['_:b3', '<step>', '"3"'],
  ['_:b3', '<description>', '"Pour white rum over ice."'],
  ['_:b2', '<step>', '"2"'],
  ['_:b2', '<description>', '"Fill glass to top with ice cubes."'],
  ['_:b1', '<step>', '"1"'],
  ['_:b1', '<description>', '"Crush lime juice, mint and sugar together in glass."']
];
global.triples = triples;

const twoIndexes = ['sp', 'so', 'ps', 'po', 'op', 'os'];

function getAggregates(state, { s, p, o }) {
  return {
    sp: state.getIn(['aggregates', 'sp', [s, p].join(',')]),
    so: state.getIn(['aggregates', 'so', [s, o].join(',')]),
    ps: state.getIn(['aggregates', 'ps', [p, s].join(',')]),
    po: state.getIn(['aggregates', 'po', [p, o].join(',')]),
    op: state.getIn(['aggregates', 'op', [o, p].join(',')]),
    os: state.getIn(['aggregates', 'os', [o, s].join(',')])
  }
}

function triple(s, p, o) {
  return { s, p, o };
}


function getPermutations(arr) {
  const permutations = [];
  const nextPermutation = [];

  function permutate(arr) {
    if (arr.length === 0) {
      permutations.push(nextPermutation.slice());
    }

    for (let i = 0; i < arr.length; i++) {
      arr.push(arr.shift());
      nextPermutation.push(arr[0]);
      permutate(arr.slice(1));
      nextPermutation.pop();
    }
  }

  permutate(arr);

  return permutations;
}

test('makeState() is an immutable collection', t => {

  const state = makeState();

  t.ok(isImmutable(state), 'is immutable');

  const expectedProperties = [
    'permutations',
    'aggregates',
    'code2term',
    'term2code',
    'clock'
  ];

  const properties = [...state.keys()];

  t.deepEqual(difference(expectedProperties, properties), [],
    `has permutations, aggregations, direct and reverse
     term maps and a logical clock counter`);

  t.equal(typeof state.get('clock'), 'number', 'clock is number');

  const permutationKeys = [...state.get('permutations').keys()];
  const permutations = getPermutations([...'spo']).map(x => x.join(''));
  t.deepEqual(difference(permutationKeys, permutations), [],
    'permutations contain all permutations of s, p, o'
  );

  const permutationValues = [...state.get('permutations').values()];
  t.ok(permutationValues.every(m => m instanceof BTMap),
    'permutations are instances immutable btree maps'
  );
  t.end();
});

test('add triple', t => {

  t.ok(add(makeState(), { s: 's', p: 'p', o: 'o' }),
    'triple can be added to a state'
  );

  const state1 = add(makeState(), { s: 's', p: 'p', o: 'o' });
  const state2 = add(state1, { s: 's', p: 'p', o: 'o' });

  t.equal(state1, state2,
    'add is idempotent'
  );

  const state = add(makeState(), { s: 's', p: 'p', o: 'o' });

  t.equal(state.get('clock'), 3,
    'clock is incremented for each added term'
  );

  t.deepEqual(state.get('term2code').toJS(), { s: 1, p: 2, o: 3 },
    'terms are mapped in term to code map'
  );

  t.deepEqual(state.get('code2term').toJS(), { 1: 's', 2: 'p', 3: 'o' },
    'codes are mapped in code to term map'
  );

  const expectedPermutationIndexContent = _(getPermutations(['s', 'p', 'o']))
    .zip(getPermutations([1, 2, 3]))
    .map(([perms, codes]) => [perms.join(''), codes])
    .value();
  t.ok(expectedPermutationIndexContent.every(([perm, code]) => state.getIn(['permutations', perm]).has(code)),
    'triple codes should appear in all permutation indices'
  );

  const singleItemAggregations = {
    s: state.getIn(['aggregates', 's', 1]),
    p: state.getIn(['aggregates', 'p', 2]),
    o: state.getIn(['aggregates', 'o', 3])
  };

  t.deepEqual(singleItemAggregations, { s: 1, p: 1, o: 1 },
    's, p, o aggregates should contain 1'
  );

  const twoItemsAggregates = getAggregates(state, triple(1, 2, 3));
  t.deepEqual(twoItemsAggregates, _(twoIndexes).map(x => [x, 1]).fromPairs().value(),
    'state contains all two-item aggregates with correct keys and values of 1'
  );

  t.end();
});

test('add two distinct triples', t => {
  const state = add(add(makeState(), triple('s1', 'p1', 'o1')), triple('s2', 'p2', 'o2'));

  t.equal(state.get('clock'), 6, 'clock increments for each added term');

  t.deepEqual(state.get('term2code').toJS(), {
    s1: 1, p1: 2, o1: 3, s2: 4, p2: 5, o2: 6
  },
    'terms are mapped in term to code map'
  );

  t.deepEqual(state.get('code2term').toJS(), {
    1: 's1', 2: 'p1', 3: 'o1', 4: 's2', 5: 'p2', 6: 'o2'
  },
    'codes are mapped in code to term map'
  );

  const tripleAggregates = state.get('aggregates').toJS();
  const oneTermAggregates = {
    s: tripleAggregates.s,
    p: tripleAggregates.p,
    o: tripleAggregates.o
  };

  const expectedIndex = {
    s: {
      1: 1,
      4: 1
    },
    p: {
      2: 1,
      5: 1
    },
    o: {
      3: 1,
      6: 1
    }
  };
  t.deepEqual(oneTermAggregates, expectedIndex,
    'in two-item aggregates all terms all relations are counted once'
  );

  const expectedTwoIndex = {
    op: {
      '3,2': 1,
      '6,5': 1
    },
    ps: {
      '2,1': 1,
      '5,4': 1
    },
    os: {
      '3,1': 1,
      '6,4': 1
    },
    so: {
      '1,3': 1,
      '4,6': 1
    },
    sp: {
      '1,2': 1,
      '4,5': 1
    },
    po: {
      '2,3': 1,
      '5,6': 1
    }
  };
  const actualTwoIndex = _.omitBy(state.get('aggregates').toJS(), (v, k) => [...'spo'].includes(k));
  t.deepEqual(actualTwoIndex, expectedTwoIndex,
    'two-term aggregates are built correctly'
  );
  t.end();
});

test('add two triples sharing object', t => {
  const state = add(add(makeState(), triple('s', 'p1', 'o1')), triple('s', 'p2', 'o2'));

  t.equal(state.get('clock'), 5, 'clock increments for each added term');
  t.end();

  t.deepEqual(state.get('term2code').toJS(), {
    s: 1, p1: 2, o1: 3, p2: 4, o2: 5
  },
    'terms are mapped in term to code map'
  );

  t.deepEqual(state.get('code2term').toJS(), {
    1: 's', 2: 'p1', 3: 'o1', 4: 'p2', 5: 'o2'
  },
    'codes are mapped in code to term map'
  );

  const tripleAggregates = state.get('aggregates').toJS();
  const oneTermAggregates = {
    s: tripleAggregates.s,
    p: tripleAggregates.p,
    o: tripleAggregates.o
  };
  t.deepEqual(oneTermAggregates, {
    s: { 1: 2 }, p: { 2: 1, 4: 1 }, o: { 3: 1, 5: 1 }
  }, 'subject will be counted in aggregates twice, and rest will appear once');
  const expectedTwoIndex = {
    op: {
      '3,2': 1,
      '5,4': 1
    },
    ps: {
      '2,1': 1,
      '4,1': 1
    },
    os: {
      '3,1': 1,
      '5,1': 1
    },
    so: {
      '1,3': 1,
      '1,5': 1
    },
    sp: {
      '1,2': 1,
      '1,4': 1
    },
    po: {
      '2,3': 1,
      '4,5': 1
    }
  };
  const actualTwoIndex = _.omitBy(state.get('aggregates').toJS(), (v, k) => ['s', 'p', 'o'].includes(k));
  t.deepEqual(actualTwoIndex, expectedTwoIndex,
    'two-term aggregate index is built correctly'
  );
});

function getStateWithMultipleTriples() {
  const empty = makeState();
  const res = triples.reduce((memo, [s, p, o]) => add(memo, { s, p, o }), empty);
  return res;
}

test('search', t => {
  const state = getStateWithMultipleTriples();
  const searchResults = [...search(state, { s: '_:b0' })]
  t.equal(searchResults.length, 14,
    'Subject search should return an exact number of triples that have the subject')

  t.deepEqual([...search(state, { s: 'foo' })], [],
    'Result set should be empty if searching for a non-existing subject'
  );

  t.deepEqual([...search(state, { s: '_:b0', p: '<ingredient>' })].map(x => x.o), [
    '"12 fresh mint leaves"',
    '"1/2 lime, juiced with pulp"',
    '"1 tablespoons white sugar"',
    '"1 cup ice cubes"',
    '"2 fluid ounces white rum"',
    '"1/2 cup club soda"'
  ], 'Subject-predicate search works');

  t.deepEqual([...search(state, { s: '_:b0', p: 'foo' })].map(x => x.o), [],
    'If searching for nonexistent property, result set should be empty'
  );

  t.equal([...search(state, { p: '<description>' })].length, 6,
    'can search by predicate'
  );

  t.deepEqual([...search(state, { p: '<description>', o: '"Mojito coctail"' })],
    [triple('_:b0', '<description>', '"Mojito coctail"')],
    '<s>can make mojito cocktail</s> can search by predicate and object'
  );

  t.deepEqual([...search(state, { o: '"Mojito coctail"' })],
    [triple('_:b0', '<description>', '"Mojito coctail"')],
    'Can search by predicate object'
  );

  t.deepEqual([...search(state, { s: '_:b0', o: '"Mojito coctail"' })],
    [triple('_:b0', '<description>', '"Mojito coctail"')],
    'Can search by object and subject'
  );
  t.end();
});

test('has', t => {
  const state = getStateWithMultipleTriples();
  t.ok(has(state,
    triple('_:b0', '<description>', '"Mojito coctail"')
  ), 'check if triple is in store');

  t.notOk(has(state,
    triple('_:nope', '<nope>', '"nooooooo"')
  ), 'returns false if triple is not in store');

  const badTriples = [
    triple('_:nope', '<description>', '"Mojito coctail"'),
    triple('_:b0', '<yep?>', '"Mojito coctail"'),
    triple('bazinga', '<yep?>', '"Mojito coctail"'),
    triple('_:b0', '<description>', '"Sensimilla"')
  ];

  t.ok(badTriples.every(triple => !has(state, triple)),
    'Return false if triples match partially'
  );

  t.end();
});

test('delete an unconnected triple', t => {
  const state = getStateWithMultipleTriples();
  const unconnected = triple('_:b31', '<unconnected>', '<node>');
  const state2 = del(state, unconnected);
  const encoded = {
    s: state.getIn(['term2code', unconnected.s]),
    p: state.getIn(['term2code', unconnected.p]),
    o: state.getIn(['term2code', unconnected.o])
  };
  const codes = [
    encoded.s,
    encoded.p,
    encoded.o
  ];

  const permutationMap = _.zip(getPermutations(['s', 'p', 'o']), getPermutations(codes))
    .map(([perms, codes]) => [perms.join(''), codes]);
  t.notOk(permutationMap.every(([perm, code]) => state2.getIn(['permutations', perm]).has(code)),
    'triple codes should be gone from all permutation indices'
  );

  const actualClock = state2.get('clock');
  const expectedClock = state.get('clock') + 3;
  t.equal(actualClock, expectedClock, 'clocks should increas my number of term changes');

  const actualCode2TermMapping = {
    s: state2.getIn(['term2code', encoded.s]),
    p: state2.getIn(['term2code', encoded.p]),
    o: state2.getIn(['term2code', encoded.o])
  };
  const expectedCode2TermMapping = {
    s: undefined,
    p: undefined,
    o: undefined
  };
  t.deepEqual(actualCode2TermMapping, expectedCode2TermMapping,
    'term references should be removed from code to term map'
  );

  const actualTerm2CodeMapping = {
    s: state2.getIn(['term2code', unconnected.s]),
    p: state2.getIn(['term2code', unconnected.p]),
    o: state2.getIn(['term2code', unconnected.o])
  };
  const expectedTerm2CodeMapping = {
    s: undefined,
    p: undefined,
    o: undefined
  };
  t.deepEqual(actualTerm2CodeMapping, expectedTerm2CodeMapping,
    'term references should be removed from code to term map'
  );


  const actualSingleTermCount = {
    s: state2.getIn(['aggregates', 's', encoded.s]),
    p: state2.getIn(['aggregates', 'p', encoded.p]),
    o: state2.getIn(['aggregates', 'o', encoded.o])
  };
  const expectedSingleTermCount = {
    s: undefined,
    p: undefined,
    o: undefined
  };
  t.deepEqual(actualSingleTermCount, expectedSingleTermCount,
    'Term counts should be deleted from the aggregation index'
  );

  const actualAggregates = getAggregates(state2, encoded);
  const expectedAggregates = {
    sp: undefined,
    so: undefined,
    ps: undefined,
    po: undefined,
    op: undefined,
    os: undefined
  };
  t.deepEqual(actualAggregates, expectedAggregates,
    'Aggregates should be removed'
  );

  t.end();
});

test('delete triples connected by subject', t => {
  const state = getStateWithMultipleTriples();
  const connected = triple('_:b0', '<yield>', '"1 cocktail"');
  const state1 = del(state, connected);
  const state2 = add(state, triple('_:b0', '<foo>', '"bar"'));
  const encoded = {
    s: state.getIn(['term2code', connected.s]),
    p: state.getIn(['term2code', connected.p]),
    o: state.getIn(['term2code', connected.o])
  };

  for (const permutation of getPermutations(['s', 'p', 'o'])) {
    const key = _([...permutation]).map(term => encoded[term]).value();
    const isKeyInStore = state1.getIn(['permutations', permutation.join('')]).has(key);
    t.notOk(isKeyInStore, `key is not in permutation index ${permutation.join('')}`);
  }

  const expectedClock = state.get('clock') + 2;
  const actualClock = state1.get('clock');
  t.equal(actualClock, expectedClock,
    'state clock should increment by 2'
  );

  const { s, p, o } = encoded;
  global.encoded = encoded;
  const join = (...x) => x.join(',');
  const actualAggregationIndices = {
    s: state1.getIn(['aggregates', 's', s]),
    p: state1.getIn(['aggregates', 'p', p]),
    o: state1.getIn(['aggregates', 'o', o]),
    sp: state1.getIn(['aggregates', 'sp', join(s, p)]),
    so: state1.getIn(['aggregates', 'so', join(s, o)]),
    ps: state1.getIn(['aggregates', 'ps', join(p, s)]),
    po: state1.getIn(['aggregates', 'po', join(p, o)]),
    op: state1.getIn(['aggregates', 'op', join(o, p)]),
    os: state1.getIn(['aggregates', 'os', join(o, s)])
  };
  const expecteAggregationIndices = {
    s: state.getIn(['aggregates', 's', s]) - 1,
    p: undefined,
    o: undefined,
    sp: undefined,
    so: undefined,
    ps: undefined,
    po: undefined,
    op: undefined,
    os: undefined
  }
  t.deepEqual(actualAggregationIndices, expecteAggregationIndices,
    'all aggregation indices should be removed, and subject index should be decremented'
  );

  global.state = state;
  global.state1 = state1;
  global.state2 = state2;
  t.end();
});
