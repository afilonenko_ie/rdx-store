import test from 'tape';
import { RDFTerm, Literal, IRI, BlankNode } from '../src/term';

test('RDFterm', t => {
  const term = new RDFTerm();
  t.ok(term, 'rdf term is initialized with new');
  t.equal(term.value, undefined, 'if constructed without params value is undefined');
  const term2 = new RDFTerm(2);
  t.equal(term2.value, 2, 'value is what passed to constructor');
  t.equal((new RDFTerm('x')).toString(), 'x',
    'uses passed value when called toString()'
  );
  t.end();
});

test('Literal', t => {
  const lit = new Literal(4);
  t.ok(lit instanceof RDFTerm, 'Literal is subclass of RDFTerm');
  t.equal(lit.value, 4, 'Value is what passed to constructor');
  t.equal(lit.termType, 'literal', 'Term type of literal is literal');
  t.equal(lit.toString(), '"4"', 'toString() returns double quoted value');
  const immutableLit = Literal.from(5);
  t.ok(immutableLit instanceof Literal, 'static from(val) returns instance of literal');
  t.ok(Object.isFrozen(immutableLit), 'static from(val) returns a frozen instance');
  t.end();
});

test('IRI', t => {
  const lit = new IRI(4);
  t.ok(lit instanceof RDFTerm, 'IRI is subclass of RDFTerm');
  t.equal(lit.value, 4, 'Value is what passed to constructor');
  t.equal(lit.termType, 'IRI', 'Term type of IRI is IRI');
  t.equal(lit.toString(), '<4>', 'toString() returns value in angular brackets');
  const immutableLit = IRI.from(5);
  t.ok(immutableLit instanceof IRI, 'static from(val) returns instance of IRI');
  t.ok(Object.isFrozen(immutableLit), 'static from(val) returns a frozen instance');
  t.end();
});

test('BlankNode', t => {
  const lit = new BlankNode(4);
  t.ok(lit instanceof RDFTerm, 'BlankNode is subclass of RDFTerm');
  t.equal(lit.value, 4, 'Value is what passed to constructor');
  t.equal(lit.termType, 'blankNode', 'Term type of BlankNode is blankNode');
  t.equal(lit.toString(), '4', 'toString() returns just value.toString()');
  const immutableLit = BlankNode.from(5);
  t.ok(immutableLit instanceof BlankNode, 'static from(val) returns instance of BlankNode');
  t.ok(Object.isFrozen(immutableLit), 'static from(val) returns a frozen instance');
  t.end();
});
